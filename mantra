#!/bin/bash

## MANTRA ###################################################
## View online manual pages from the terminal. ##############

#############################################################
## VARIABLES ################################################

baseurl="https://man.archlinux.org"
tmpfile="/tmp/mantra.html"

#############################################################
## FUNCTIONS ################################################

helpinfo() {
  cat << EOF
Usage: mantra [OPTION]...
View online manual pages (from https://man.archlinux.org) from the terminal.

  -k <query>  keyword
  -h          display this help and exit

EOF
}

depcheck() {
	for dep; do
    if [[ -z $(command -v "$dep") ]]; then
			printf "%s not found. Please install it.\n" "$dep" >&2
			exit 2
		fi
	done
	unset dep
}

fetch() {
  title=$(pup -f $tmpfile 'section:nth-child(2) a:nth-child(1) text{}') 
  links=$(pup -f $tmpfile 'section:nth-child(2) a:nth-child(1) attr{href}') 
  
  paste <(printf '%s\n' $title) <(printf '%s\n' $links) |\
    fzf --bind "enter:execute(man <(curl -s $baseurl/{2}.raw))" --bind enter:+close
}

#############################################################

depcheck "curl" "pup" "fzf"

while [ "$1" != "" ]; do
    case $1 in
    -k | --keyword) shift; keyword=$1 ;; 
    -h | --help) helpinfo && exit ;; 
    *) printf '%s\n\n' "Invalid option." && helpinfo && exit ;;
    esac
    shift
done

while [[ -z $keyword ]]
do
  read -rp "Enter keyword: " keyword
done

curl -s "$baseurl/search?q=$keyword&lang=en" > $tmpfile

fetch
