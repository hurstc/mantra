# mantra

![mantra in action.](.assets/mantra.mp4)

View online manual pages (from https://man.archlinux.org) from the terminal.

## Dependencies

- [curl](https://curl.se/)
- [fzf](https://github.com/junegunn/fzf)
- [pup](https://github.com/EricChiang/pup)

## Usage

```
Usage: mantra [OPTION]...

  -k <query>  keyword
  -h          display this help and exit
```

## Installation

1. Clone the git repository: `git clone https://gitlab.com/chanceboudreaux/mantra.git`
2. Make the script executable: `chmod +x mantra/mantra`
3. Move the script to $PATH: `sudo mv mantra/mantra /usr/local/bin/`
